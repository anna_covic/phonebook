package phonebook;

public class Contact {
	
	private String name;
	private String number;
	private String email;
	
	public Contact(String name, String number, String email) {
		this.name = name;
		this.number = number;
		this.email = email;
	}

	public String getName() {
		return name;
	}

	public String getNumber() {
		return number;
	}


	public String getEmail() {
		return email;
	}
	

	public String toString() {
		
		return "Contact name:\t" + name + "\tPhone number:\t" + number +
				"\tE-mail adress:\t" + email;
	}
}
