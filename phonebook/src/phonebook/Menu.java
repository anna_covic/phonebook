package phonebook;

public class Menu {
	
	public void showMenu() {
		
		System.out.println("This is your phonebook. What would you like to do?"
				+ "\nPlease enter the number of the desired action: "
				+ "\nList my contacts: 1"
				+ "\nAdd new contact: 2"
				+ "\nDelete contact: 3"
				+ "\nExit program: 4");
	}
	
}
