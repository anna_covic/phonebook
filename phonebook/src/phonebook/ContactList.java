package phonebook;

import java.util.ArrayList;

public class ContactList {
	

	private ArrayList<Contact> phonebook = new ArrayList<Contact>();
	
	
	public void addNew(String name, String number, String email) {
		Contact person = new Contact(name, number, email);
		this.phonebook.add(person);
		
	}
	
	public void printAll() {
		for (Contact person : this.phonebook) {
			System.out.println(person);
		}
	}
	
	public String search(String name) {
		for (Contact person : this.phonebook) {
			if (person.getName().equals(name)) {
				return person.getNumber();
			}
		}
		return "Contact not found, please try again.";
	}
	


}
